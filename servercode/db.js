const mysql = require('mysql2')

// pool (collection) of connections
const pool = mysql.createPool({
  host: 'shivangi',
  user: 'root',
  password: 'root',
  database: 'mydb2',
  port: 3306,

  // number of connections opened at a time
  connectionLimit: 20,
})

module.exports = {
  pool,
}
