const express = require('express')
const utils = require('../utils')
const db = require('../db')
const router = express.Router()

router.post('/addbook', (req, res) => {
  const { book_id, book_title, publisher_name, author_name } = req.body
  console.log(book_id)
  const statement = `insert into Book( book_id,book_title, publisher_name, author_name) values(?,?,?,?);`
  db.pool.query(
    statement,
    [book_id, book_title, publisher_name, author_name],
    (err, data) => {
      res.send(utils.createResult(err, data))
    }
  )
})
router.get('/getallbook', (req, res) => {
  //to get all cars on home screen
  const statement = `select * from Book`
  db.pool.query(statement, (err, data) => {
    res.send(utils.createResult(err, data))
  })
})

module.exports = router
